/* SPDX-License-Identifier: GPL-2.0 */

#include <linux/gpio/consumer.h>
#include <linux/device.h>
#include <linux/errno.h>
#include <linux/i2c.h>
#include <linux/input.h>
#include <linux/module.h>
#include <linux/gpio.h>

#define NATE_DRV_NAME "nate"
#define NATE_INPUT_NAME (NATE_DRV_NAME "-keyboard")
#define NATE_IRQ_NAME (NATE_DRV_NAME "-irq")

#define NATE_TX_MAX_RETRIES 3
#define NATE_PIN_MASK U16_C(0xffff)

enum {
	/* PI4IOE5V6416 IO Expander */
	PI4IO16_REG_INPUT_0 = 0x00,
	PI4IO16_REG_INPUT_1 = 0x01,
	PI4IO16_REG_CONFIGURATION_0 = 0x06,
	PI4IO16_REG_CONFIGURATION_1 = 0x07,
	PI4IO16_REG_INPUT_LATCH_0 = 0x44,
	PI4IO16_REG_INPUT_LATCH_1 = 0x45,
	PI4IO16_REG_PULL_UP_PULL_DOWN_ENABLE_0 = 0x46,
	PI4IO16_REG_PULL_UP_PULL_DOWN_ENABLE_1 = 0x47,
	PI4IO16_REG_PULL_UP_PULL_DOWN_SELECT_0 = 0x48,
	PI4IO16_REG_PULL_UP_PULL_DOWN_SELECT_1 = 0x49,
	PI4IO16_REG_INTERRUPT_MASK_0 = 0x4a,
	PI4IO16_REG_INTERRUPT_MASK_1 = 0x4b,
};

struct nate_data {
	struct i2c_client *client;
	struct input_dev *idev;
	struct gpio_desc *irq_pin;
	struct gpio_desc *backlight_pin;
	unsigned int irq;
	u16 key_state;
	u8 backlight_state;
};

/* NOTE(José): This exact order of keys should be used because it matches the pin numbering on the board. */
static const int nate_key_codes[] = {
	[0] = KEY_6,	      [1] = KEY_1,	[2] = KEY_7,
	[3] = KEY_2,	      [4] = KEY_3,	[5] = KEY_8,
	[6] = KEY_4,	      [7] = KEY_9,	[8] = KEY_5,
	[9] = KEY_0,	      [10] = KEY_COMMA, [11] = KEY_ENTER,
	[12] = KEY_BACKSPACE, [13] = KEY_ESC,	[14] = KEY_DOWN,
	[15] = KEY_UP,
};

static ssize_t backlight_show(struct device *dev, struct device_attribute *attr,
			      char *buf)
{
	struct nate_data *data = dev_get_drvdata(dev);
	return sprintf(buf, "%u\n", data->backlight_state);
}

static ssize_t backlight_store(struct device *dev,
			       struct device_attribute *attr, const char *buf,
			       size_t count)
{
	struct nate_data *data;
	int ret;
	u8 backlight;

	data = dev_get_drvdata(dev);

	ret = kstrtou8(buf, 0, &backlight);
	if (ret == 0 && backlight != data->backlight_state) {
		gpiod_set_value(data->backlight_pin, backlight);
		data->backlight_state = backlight;
	}

	return strnlen(buf, count);
}

static DEVICE_ATTR_RW(backlight);

static struct attribute *nate_sysfs_entries[] = {
	&dev_attr_backlight.attr,
	NULL,
};

static const struct attribute_group nate_attribute_group = {
	.attrs = nate_sysfs_entries,
};

static int nate_i2c_write_byte(struct i2c_client *client, u8 reg, u8 byte)
{
	struct i2c_msg msg;
	int retries = 0, ret = 0;
	u8 buffer[2];

	buffer[0] = reg;
	buffer[1] = byte;

	msg.addr = client->addr;
	msg.flags = 0;
	msg.buf = buffer;
	msg.len = sizeof(buffer);

	do {
		ret = i2c_transfer(client->adapter, &msg, 1);
		++retries;
	} while (ret != 1 && retries < NATE_TX_MAX_RETRIES);

	if (ret <= 0)
		ret = -EIO;

	return ret;
}

static int nate_i2c_read_byte(struct i2c_client *client, u8 reg, u8 *byte)
{
	struct i2c_msg msgs[2];
	int retries = 0, ret = 0;

	msgs[0].addr = client->addr;
	msgs[0].flags = 0;
	msgs[0].buf = &reg;
	msgs[0].len = sizeof(reg);

	msgs[1].addr = client->addr;
	msgs[1].flags = I2C_M_RD;
	msgs[1].buf = byte;
	msgs[1].len = sizeof(*byte);

	do {
		ret = i2c_transfer(client->adapter, msgs, 2);
		++retries;
	} while (ret != 2 && retries < NATE_TX_MAX_RETRIES);

	if (ret <= 0)
		ret = -EIO;

	return ret;
}

static int nate_i2c_write_word(struct i2c_client *client, u8 reg, u16 word)
{
	int ret;
	u8 lo, hi;

	lo = word & 0xff;
	hi = word >> 8;

	ret = nate_i2c_write_byte(client, reg, lo);
	if (ret < 0)
		return ret;

	ret = nate_i2c_write_byte(client, reg + 1, hi);
	return ret;
}

static int nate_i2c_read_word(struct i2c_client *client, u8 reg, u16 *word)
{
	int ret;
	u8 lo, hi;

	ret = nate_i2c_read_byte(client, reg, &lo);
	if (ret < 0)
		goto out;

	ret = nate_i2c_read_byte(client, reg + 1, &hi);
	if (ret < 0)
		goto out;

	*word = (hi << 8) | lo;

out:
	return ret;
}

static int nate_input_open(struct input_dev *idev)
{
	struct nate_data *data = input_get_drvdata(idev);
	enable_irq(data->irq);
	return 0;
}

static void nate_input_close(struct input_dev *idev)
{
	struct nate_data *data = input_get_drvdata(idev);
	disable_irq(data->irq);
}

static int nate_input_init(struct nate_data *data)
{
	struct input_dev *idev = input_allocate_device();
	if (!idev)
		return -ENOMEM;

	idev->name = NATE_INPUT_NAME;
	idev->open = nate_input_open;
	idev->close = nate_input_close;

	idev->dev.parent = &data->client->dev;
	idev->id.bustype = BUS_I2C;

	for (int i = 0; i < ARRAY_SIZE(nate_key_codes); ++i)
		input_set_capability(idev, EV_KEY, nate_key_codes[i]);

	set_bit(EV_REP, idev->evbit);

	data->idev = idev;
	input_set_drvdata(idev, data);

	return input_register_device(idev);
}

static void nate_scan_keys(struct nate_data *data)
{
	int count = 0, ret;
	u16 new_key_state, changed_pins;

	ret = nate_i2c_read_word(data->client, PI4IO16_REG_INPUT_0,
				 &new_key_state);
	if (ret < 0) {
		dev_err(&data->client->dev, "failed to read in irq");
		return;
	}

	new_key_state &= NATE_PIN_MASK;
	changed_pins = data->key_state ^ new_key_state;
	data->key_state = new_key_state;

	if (!changed_pins)
		return;

	for (int i = 0; i < ARRAY_SIZE(nate_key_codes); ++i) {
		u16 state;

		if (!(NATE_PIN_MASK & BIT(i)))
			continue;

		if (changed_pins & BIT(i)) {
			state = new_key_state & BIT(i);
			input_report_key(data->idev, nate_key_codes[i], state);
			++count;
		}
	}

	if (count)
		input_sync(data->idev);
}

static irqreturn_t nate_irq_handler(int irq, void *dev_id)
{
	struct nate_data *data = dev_id;
	nate_scan_keys(data);
	return IRQ_HANDLED;
}

static int nate_backlight_init(struct nate_data *data)
{
	int ret = 0;

	data->backlight_state = 1;
	data->backlight_pin =
		gpiod_get(&data->client->dev, "natebacklight", GPIOD_OUT_HIGH);
	if (IS_ERR(data->backlight_pin)) {
		ret = PTR_ERR(data->backlight_pin);
		data->backlight_pin = NULL;
		dev_err(&data->client->dev,
			"Failed to acquire backlight pin: %i\n", ret);
	}

	return ret;
}

static int nate_irq_init(struct nate_data *data)
{
	int ret;

	data->irq_pin = gpiod_get(&data->client->dev, "nateirq", GPIOD_ASIS);
	if (IS_ERR(data->irq_pin)) {
		ret = PTR_ERR(data->irq_pin);
		data->irq_pin = NULL;
		dev_err(&data->client->dev, "Failed to acquire irq pin: %i\n",
			ret);
		goto out;
	}

	ret = gpiod_to_irq(data->irq_pin);
	if (ret < 0) {
		dev_err(&data->client->dev,
			"Failed to get irq from gpiod: %i\n", ret);
		goto out;
	}

	data->irq = (unsigned int)ret;

	ret = request_threaded_irq(data->irq, NULL, nate_irq_handler,
				   IRQF_TRIGGER_FALLING | IRQF_ONESHOT |
					   IRQF_NO_AUTOEN,
				   NATE_IRQ_NAME, data);

out:
	if (ret < 0 && data->irq_pin) {
		gpiod_put(data->irq_pin);
		data->irq_pin = NULL;
	}

	return ret;
}

static void nate_irq_destroy(struct nate_data *data)
{
	free_irq(data->irq, data);
	gpiod_put(data->irq_pin);
	data->irq_pin = NULL;
}

static void nate_backlight_destroy(struct nate_data *data)
{
	gpiod_put(data->backlight_pin);
	data->backlight_pin = NULL;
}

static int nate_data_init(struct nate_data *data, struct i2c_client *client)
{
	int ret;

	data->client = client;

	ret = nate_irq_init(data);
	if (ret < 0) {
		dev_err(&data->client->dev, "Failed to initialize irq: %i\n",
			ret);
		goto out;
	}

	ret = nate_backlight_init(data);
	if (ret < 0) {
		nate_irq_destroy(data);
		goto out;
	}

	ret = nate_input_init(data);
	if (ret < 0) {
		nate_backlight_destroy(data);
		nate_irq_destroy(data);
		dev_err(&data->client->dev,
			"Failed to initialize input device: %i\n", ret);
	}

out:
	return ret;
}

static int nate_i2c_write_word_and_verify(struct i2c_client *client, u8 reg,
					  u16 word)
{
	int ret;
	u16 written;

	ret = nate_i2c_write_word(client, reg, word);
	if (ret < 0)
		goto out;

	ret = nate_i2c_read_word(client, reg, &written);
	if (ret < 0)
		goto out;

	if (written != word)
		ret = -EIO;

out:
	return ret;
}

static int nate_device_setup(struct i2c_client *client)
{
	int ret;

	/* Configure input pins */
	ret = nate_i2c_write_word_and_verify(
		client, PI4IO16_REG_CONFIGURATION_0, NATE_PIN_MASK);
	if (ret < 0)
		goto out;

	/* Enable pull down / pull up */
	ret = nate_i2c_write_word_and_verify(
		client, PI4IO16_REG_PULL_UP_PULL_DOWN_ENABLE_0, NATE_PIN_MASK);
	if (ret < 0)
		goto out;

	/* Select pull down */
	ret = nate_i2c_write_word_and_verify(
		client, PI4IO16_REG_PULL_UP_PULL_DOWN_SELECT_0,
		(u16)~NATE_PIN_MASK);
	if (ret < 0)
		goto out;

	/* Enable the latch on input pins */
	ret = nate_i2c_write_word_and_verify(client, PI4IO16_REG_INPUT_LATCH_0,
					     NATE_PIN_MASK);
	if (ret < 0)
		goto out;

	/* Unmask interrupts */
	ret = nate_i2c_write_word_and_verify(
		client, PI4IO16_REG_INTERRUPT_MASK_0, (u16)~NATE_PIN_MASK);
	if (ret < 0)
		goto out;

out:
	return ret;
}

static int nate_i2c_probe(struct i2c_client *client)
{
	struct nate_data *data;
	int ret;

	ret = i2c_check_functionality(client->adapter, I2C_FUNC_I2C);
	if (!ret)
		return -EIO;

	ret = nate_device_setup(client);
	if (ret < 0) {
		dev_err(&client->dev, "Failed to setup the device: %i\n", ret);
		return ret;
	}

	data = devm_kzalloc(&client->dev, sizeof(struct nate_data), GFP_KERNEL);
	if (!data)
		return -ENOMEM;

	i2c_set_clientdata(client, data);
	dev_set_drvdata(&client->dev, data);

	ret = sysfs_create_group(&client->dev.kobj, &nate_attribute_group);
	if (ret < 0)
		return ret;

	ret = nate_data_init(data, client);
	if (ret < 0)
		dev_err(&data->client->dev,
			"Failed to initialize nate data: %i\n", ret);

	return ret;
}

static const struct of_device_id nate_dt_ids[] = {
	{
		.compatible = "42we," NATE_DRV_NAME,
	},
	{},
};
MODULE_DEVICE_TABLE(of, nate_dt_ids);

static struct i2c_driver nate_i2c_driver = {
	.driver = { .name = NATE_DRV_NAME, .of_match_table = nate_dt_ids, .owner = THIS_MODULE, },
	.probe_new = nate_i2c_probe,
};

module_i2c_driver(nate_i2c_driver);

MODULE_DESCRIPTION("NATE board driver");
MODULE_LICENSE("GPL");
MODULE_AUTHOR("José Guilherme <jose.guilherme.cr.bh@proton.me>");
