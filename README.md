# NATE Module

## Cross-Compilation and Installation

The compilation should be pretty similar for most cases. The installation is shown as done
in a Raspberry PI 4 Model B.

Cross-compile module:
```bash
pushd module

export KDIR=/home/djouze/linux
export ARCH=arm64
export CROSS_COMPILE=aarch64-linux-gnu-

make -j
popd
```

Install module:
```bash
pushd module

export KDIR=/home/djouze/linux
export ARCH=arm64
export CROSS_COMPILE=aarch64-linux-gnu-

$ sudo -E make -C $KDIR M=$(pwd) ARCH=$ARCH CROSS_COMPILE=$CROSS_COMPILE INSTALL_MOD_PATH=/run/media/djouze/rootfs modules_install

popd
```


Compile overlay:
```bash
pushd module

make dtbo

popd
```

Install overlay:
```bash
pushd module

cp nate.dtbo /run/media/djouze/bootfs/overlays

popd
```

##  Generate compile_commands.json for module

Use [Bear](https://github.com/rizsotto/Bear).
```bash
$ bear -- make -j
```

clangd will complain about some flags, so keep in mind any such flags must be placed in the `.clangd` file,
as seen in: https://github.com/clangd/clangd/issues/1653
```
CompileFlags:
    Remove: [-fconserve-stack, -fno-allow-store-data-races, -mabi=lp64]

```
