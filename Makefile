obj-m += src/nate.o

KDIR ?= /lib/modules/$(shell uname -r)/build

all:
	make -C $(KDIR) M=$(PWD) modules

modules_install:
	make -C $(KDIR) M=$(PWD) modules_install

dtbo:
	dtc -@ -I dts -O dtb -o nate.dtbo nate.dts

clean:
	make -C $(KDIR) M=$(PWD) clean
